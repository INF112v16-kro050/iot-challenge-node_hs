module Main where

import Data.List
import Data.Char
import Control.Exception
import Data.Maybe (isJust, isNothing, fromJust, fromMaybe)

main = repl
-- Starts a REPL for the language. It does what REPLs do
repl = locRepl emptyCtx emptyMem
  where locRepl context memory = do
          input <- getLine
          result <- try (evaluate( eval ( parse input) context memory))
                 :: IO (Either SomeException (Ast, Context, Memory))
          case result of
            Left ex -> do putStrLn $ (show ex)
                          locRepl context memory
            Right (ast, new_cont, new_mem) -> do putStrLn $ (show ast)
                                                 locRepl new_cont new_mem

compoundOps = ["==", "!=", "->"]

intops = [('+', (+)), ('-', (-)), ('*', (*)), ('/', div)]

boolops :: [(Char, Integer -> Integer -> Bool)]
boolops = [('<', (<)), ('>', (>))]

boolfuncs =  [("<", (<)), (">", (>)), ("==", (==)), ("!=", (/=))]

delimiters = [';', ',', ')', '(', '.']

tokenize :: String -> [String]
tokenize [] = []
tokenize (ch:s) | isAlpha ch = let (str, rem) = span isAlpha (ch:s)
                               in [str] ++ (tokenize rem)
tokenize (ch:s) | isDigit ch = let (str, rem) = span isDigit (ch:s)
                               in [str] ++ (tokenize rem)
tokenize xs | not $ null $ filter (\s -> isPrefixOf s xs) compoundOps = let (op, rem) = splitAt 2 xs
                                                                     in [op] ++ (tokenize rem)
tokenize (ch:s) | (isJust $ lookup ch intops) || (isJust $ lookup ch boolops) = [[ch]] ++ (tokenize s)
tokenize (ch:s) | ch `elem` delimiters = [[ch]] ++ (tokenize s)
tokenize (ch:s) = tokenize s

data Ast =  Number Integer | Name String | App Ast [Ast] | Block [Ast] |
            Case Ast [Ast] | Bool Ast Ast Ast | Default | Set String Ast |
            Lambda String Ast | Function String Ast Context
         deriving (Eq,Show)

parseErr expected found = error ("Parse error: Expected \"" ++ expected ++ "\", found \"" ++ found ++ "\".")
parseAssert :: String -> String -> ()
parseAssert expected found = if expected /= found
                             then parseErr expected found
                             else ()

type Tokens = [String]

parseExp :: Tokens -> (Ast, Tokens)
parseExp (x:xs) | isDigit $ head x = (Number (read x), xs)
parseExp ("(":xs) = parseApp $ "(":xs
parseExp ("case":xs) = parseCase $ "case":xs
parseExp ("set":xs) = parseSet $ "set":xs
parseExp ("lambda":xs) = let (ast1, rem1) = parseLambda $ "lambda":xs
                             (ast2, closePar:rem2) = parseExp $ tail rem1
                         in if (head rem1) == "("
                            then seq (parseAssert ")" closePar) (App ast1 [ast2], rem2)
                            else (ast1, rem1)
parseExp (x:xs) | isLetter $ head x =
  let (ast, closePar:rem) = parseExp $ tail xs
  in if head xs == "("
     then seq (parseAssert ")" closePar) $ (App (Name x) [ast], rem)
     else (Name x, xs)
parseExp (x:xs) = parseErr "expression" x
parseExp [] = parseAssert "expression" "eol" `seq` error ""

parseLambda :: Tokens -> (Ast, Tokens)
parseLambda ("lambda":x:openPar:xs) =
  let (ast, closePar:rem) = parseExp xs
  in seq (parseAssert "(" openPar) $
     seq (parseAssert ")" closePar)
     (Lambda x ast, rem)

parseSet :: Tokens -> (Ast, Tokens)
parseSet ("set":x:xs) = let (ast, rem) = parseExp xs
                        in (Set x ast, rem)

parseApp :: Tokens -> (Ast, Tokens)
parseApp ("(":xs) = let (ast1, comma:rem1) = parseExp xs
                        (ast2, closePar:op:rem2) = parseExp rem1
                    in if comma /= "," then parseErr "," comma
                       else if closePar /= ")" then parseErr ")" closePar
                            else if isNothing $ lookup (head op) intops
                                 then error ("Illegal operator " ++ op)
                                 else (App (Name op) [ast1, ast2], rem2)

parseBool :: Tokens -> (Ast, Tokens)
parseBool (openPar:xs) = let (ast1, comma:rem1) = parseExp xs
                             (ast2, closePar:op:rem2) = parseExp rem1
                         in (parseAssert "," comma) `seq` (parseAssert ")" closePar) `seq`
                            (parseAssert "(" openPar) `seq`
                            if isJust $ lookup op boolfuncs
                            then ((Bool (Name op) ast1 ast2), rem2)
                            else error ("Illegal operator " ++ op)

parseCase :: Tokens -> (Ast, Tokens)
parseCase ("case":"otherwise":arrow:xs) =
  let (ast, stop:rem) = parseExp xs
  in (parseAssert "." stop) `seq` (parseAssert "->" arrow) `seq` (Case Default [ast], rem)
parseCase ("case":xs) =
  let (ast1, arrow:rem1) = parseBool xs
      (ast2, comma:rem2) = parseExp rem1
      (ast3, rem3) = parseCase rem2
  in (parseAssert "->" arrow) `seq` (parseAssert "," comma) `seq` (Case ast1 [ast2, ast3], rem3)
                                  
                            
parseBlock :: Tokens -> (Ast, Tokens)
--parseBlock [] = parseErr ";" "eol"
parseBlock [] = (Block [], [])
parseBlock xs = let (expr, rem1) = parseExp xs
                    (semicolon:rem2) = if null rem1
                                       then error "Missing semicolon at end of block"
                                       else rem1
                    (Block ys, rem3) = parseBlock rem2
                in (parseAssert ";" semicolon) `seq` (Block (expr:ys), rem3)


type Memory = (Int, Int -> Maybe Ast)
newtype Context = Context (String -> Maybe Int)

instance Show Context where
  show _ = ""
instance Eq Context where
  _ == _ = False

emptyMem = (0, const Nothing)
emptyCtx = Context (const Nothing)

store :: String -> Ast -> Context -> Memory -> (Context, Memory)
store str ast con (nextAddress, mem) = (addToCtx str nextAddress con,
                                        addToMem ast (nextAddress, mem))

lookupVar :: String -> Context -> Memory -> Maybe Ast
lookupVar str (Context con) (_, mem) = (con str) >>= mem
  
addToMem :: Ast -> Memory -> Memory
addToMem ast (address, mem) = (address + 1, (\key -> if key == address
                                                     then Just ast else mem (key)))
mutMem :: Int -> Ast -> Memory -> Memory
mutMem address ast (nextAddress, mem) = (nextAddress, (\key -> if key == address
                                                              then Just ast else mem (key)))

addToCtx :: String -> Int -> Context-> Context
addToCtx str n (Context con) = Context (\key -> if str == key then Just n else con key)

-- Evaluates a list of ASTs, updating the context/memory for each AST as it goes though the list
evals :: [Ast] -> Context -> Memory -> ([Ast], Context, Memory)
evals [] con mem = ([], con, mem)
evals (x:xs) con mem = let (ast, newCon, newMem) = eval x con mem
                           (asts, finalCon, finalMem) = evals xs newCon newMem
                       in (ast:asts, finalCon, finalMem)

eval :: Ast -> Context -> Memory -> (Ast, Context, Memory)
eval (Lambda var ast) con mem = (Function var ast con, con, mem)

eval (Set name ast) con mem =
  let (evaledAst, (Context newCon1), newMem1) = eval ast con mem
      (newCon2, newMem2) = if isJust $ newCon1 name -- If variable is already defined
                           then (Context newCon1, mutMem (fromJust $ newCon1 name) evaledAst newMem1)
                           else store name evaledAst (Context newCon1) newMem1
  in (evaledAst, newCon2, newMem2)

eval (Name var) con mem = case lookupVar var con mem of
  Just ast -> (ast, con, mem)
  Nothing -> error ("Variable " ++ var ++ " not in scope.")

eval (Block [x]) con mem = eval x con mem
eval (Block xs) con mem = let (asts, newCon, newMem) = evals xs con mem
                          in (last asts, newCon, newMem)
                             
eval (Number n) con mem = ((Number n), con, mem)

-- Evaluating lambdas stored in memory
eval (App (Name op) [ast]) con mem =
  case lookupVar op con mem of
    Just func@(Function _ _ _) ->
      let (result, newCon, newMem) = eval (App func [ast]) con mem
      in (result, newCon, newMem)
    Just x -> error (show x)
    Nothing -> error ("Function " ++ op ++ " not in scope.")

-- Evaluating lambdas applied directly
-- Lambdas probably should not have their parameter in scope right away, instance
-- case it gets substituted later
eval (App (Lambda par body) xs) con mem = eval (App (Function par body con) xs) con mem

eval (App (Function parameter body funCon) [Name arg]) con mem =
  let newBody = replace arg parameter body
      Context outer_context = con
      (ast, _, newMem) = eval newBody (addToCtx arg (fromJust $ outer_context arg) funCon) mem
  in (ast, con, newMem)

eval (App (Function parameter body funCon1) [arg]) con mem =
  let (evaledArg, newCon, newMem1) = eval arg con mem
      (funCon2, newMem2) = store parameter evaledArg funCon1 newMem1
      (ast, _, newMem3) = eval body funCon2 newMem2
  in (ast, newCon, newMem3)

eval (App (Name op) [ast1, ast2]) con mem =
  let ([numAst1, numAst2], newCon, newMem) = evals [ast1, ast2] con mem
      ([Number n1, Number n2]) =
        case (numAst1, numAst2) of
          (Number n1, Number n2) -> [numAst1, numAst2]
          (a, b) -> error ("Error: tried to apply " ++ op ++ " to non-numbers \"" ++ (show a) ++ "\" and \"" ++ (show b) ++ "\"")
  in (Number ((fromJust $ lookup (head op) intops) n1 n2), newCon, newMem)
  
eval (Case Default [ast]) con mem = eval ast con mem
eval (Case (Bool (Name op) ast1 ast2) [body, nextCase]) con mem =
  let ([Number n1, Number n2], newCon, newMem) = evals [ast1, ast2] con mem
  in if (fromJust $ lookup op boolfuncs) n1 n2
     then eval body newCon newMem
     else eval nextCase newCon newMem

eval xs _ _ = error ("Couldn't evaluate " ++ (show xs) ++ ".")

replace new old (Block xs) = Block $ map (replace new old) xs 
replace new old (Name var) = if old == var then Name new else Name var
replace _ _ n@(Number _) = n
replace new old (App ast asts) = App (replace new old ast) $ map (replace new old) asts
replace new old (Case ast asts) = Case (replace new old ast) $ map (replace new old) asts
replace new old (Bool ast1 ast2 ast3) = Bool (replace new old ast1) (replace new old ast2) (replace new old ast3)
replace _ _ Default = Default
replace new old (Set var ast) = if old == var then Set new (replace new old ast) else Set var ast
replace _ _ l@(Lambda _ _) = l
replace _ _ f@(Function _ _ _) = f

parse = fst . parseBlock . tokenize

runEval s = let (result, con, mem) = eval (parse $ prelude++s) emptyCtx emptyMem
            in (result, map (\(a, b) -> (a, fromJust b)) $ filter (isJust . snd) $ printConMem "a" con mem)

run s = let (ast, _, _) = eval (parse $ prelude++s) emptyCtx emptyMem in ast

prelude = "set zero lambda x (0); set id lambda x (x); set f 0;"

--printContMem con mem 

printConMem :: String -> Context -> Memory -> [(String, Maybe Ast)]
printConMem str c@(Context con) m@(_, mem) =
  if next str /= Nothing
  then (str, con str >>= mem):(printConMem (fromJust $ next str) c m)
  else [(str, con str >>= mem)]

next :: String -> Maybe String
next (x:[]) | x == 'Z' = Just "a"
next (x:[]) | x == 'z' = Nothing
next (x:[]) = Just $ [succ x]
next (x:xs) | x == 'Z' && next xs == Nothing = Just ('a':xs)
next (x:xs) | x == 'z' && next xs == Nothing = Nothing
next (x:xs) | next xs == Nothing = Just $ (succ x):xs
next (x:xs) = Just $ x:(fromJust $ next xs)


-- run "set f 0; set pow lambda x (lambda y (case (y, zero(set thunk f((x, 0)-)))== -> x, case otherwise -> f(thunk((y, 1)-)).)); set f lambda x (pow(x)); set powtwo pow(2); powtwo(2);"

-- "set f 0; set pow lambda x (lambda y (case (y, zero(set thunk f(x)))== -> 1, case otherwise -> (x, thunk((y, 1)-))*.)); set f lambda x (pow(x)); set p pow(2); p(20);"

