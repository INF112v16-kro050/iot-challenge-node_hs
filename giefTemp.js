var SensorTag = require('sensortag');

var log = function(text) {
  if(text) {
    console.log(text);
  }
}


var ADDRESS = "b0:b4:48:c9:74:80";
var connected = new Promise((resolve, reject) => SensorTag.discoverByAddress(ADDRESS, (tag) => resolve(tag)))
  .then((tag) => new Promise((resolve, reject) => tag.connectAndSetup(() => resolve(tag))));


//Enable sensors
var sensor = connected.then(function(tag) {
    log("connected");

    tag.enableIrTemperature(log);
    tag.notifyIrTemperature(log);

    tag.enableHumidity(log);
    tag.notifyHumidity(log);

    tag.enableGyroscope(log);
    tag.notifyGyroscope(log);

       
      
    tag.enableAccelerometer(log); 
    tag.notifyAccelerometer(log); 

    return tag;
});


// A simple example of an act on the humidity sensor.
var prev = 0;
sensor.then(function(tag) {
  tag.on("humidityChange", function(temp, humidity){
    if(prev < 35 && humidity > 35) {
      log("Don't slobber all over the SensorTag please...");
    }
    prev = humidity;
  });
});





// A simple example of an act on the irTemperature sensor.
sensor.then(function(tag) {
  tag.on("irTemperatureChange", function(objectTemp, ambientTemp) {
    log(objectTemp);
  })
});

sensor.then(function(tag) {
  tag.on("accelerometerChange", function(x, y, z) {
    //log("I'm getting queasy");
  })
});



sensor.then(function(tag) {
  tag.on("irTemperatureChange", function(objectTemp, ambientTemp) {
	if(objectTemp > 37)    
		log("You are a very sick person");
  })
});

