var SensorTag = require('sensortag');

var n = 0;

var log = function(text) {
  if(text) {
    n += 1;
    if (n % 3 == 0) {
      console.log(text);
      speak(text);
    }
  }
}

var sys = require('sys')

var exec = require('child_process').exec;


function speak(output) {
  exec("espeak \"" + output + " \"", function (error, stdout, stderr) {

    sys.print('stdout: ' + stdout);

    sys.print('stderr: ' + stderr);

    if (error !== null) {

      console.log('exec error: ' + error);

  }

  });

}


/*
child = exec("espeak Reading: \"" + out + "\" ", function (error, stdout, stderr) {

  sys.print('stdout: ' + stdout);

  sys.print('stderr: ' + stderr);

  if (error !== null) {

    console.log('exec error: ' + error);

  }

});
*/

var ADDRESS = "b0:b4:48:c9:74:80";
var connected = new Promise((resolve, reject) => SensorTag.discoverByAddress(ADDRESS, (tag) => resolve(tag)))
  .then((tag) => new Promise((resolve, reject) => tag.connectAndSetup(() => resolve(tag))));


//Enable sensors
var sensor = connected.then(function(tag) {
    log("connected");

    tag.enableIrTemperature(log);
    tag.setIrTemperaturePeriod(1000, log);
    tag.notifyIrTemperature(log);

    tag.enableHumidity(log);
    tag.notifyHumidity(log);

    tag.enableGyroscope(log);
    tag.notifyGyroscope(log);
      

    return tag;
});


// A simple example of an act on the humidity sensor.
var prev = 0;
sensor.then(function(tag) {
  tag.on("humidityChange", function(temp, humidity){
    if(prev < 35 && humidity > 35) {
      log("Don't slobber all over the SensorTag please...");
    }
    prev = humidity;
  });
});





// A simple example of an act on the irTemperature sensor.
sensor.then(function(tag) {
  tag.on("irTemperatureChange", function(objectTemp, ambientTemp) {
    log(objectTemp);
  })
});

sensor.then(function(tag) {
  tag.on("irTemperatureChange", function(objectTemp, ambientTemp) {
	if(objectTemp > 37)    
		log("You are a very sick person");
  })
});

